package hellopackage;
import secondpackage.Utilities;
import java.util.Scanner;
import java.util.Random;
public class Greeter {
    public static void main(String[] args) {
        System.out.println("app starts here::");
        Scanner object = new Scanner(System.in);
        System.out.println("please input a number and I will double it for you: ");
        int userInput = object.nextInt();
        Utilities newUtility = new Utilities();
        System.out.println("you inserted " + userInput + " therefore " + userInput + " * 2 is equal to: " + newUtility.doubleMe(userInput));
    }
}